#!/bin/bash
echo "Creating jenkins HOME directory"
mkdir $HOME/jenkins -p
mkfifo $HOME/jenkins/pipe
nohup bash ./pipe.sh &
echo "Deploying jenkins docker container"
docker-compose -f jenkins_docker-compose.yml up -d
