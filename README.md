# README #
# OS Version: Ubuntu 20.04 #
# STEP-1 Install Docker #
Follow this instruction https://docs.docker.com/engine/install/ubuntu/

After done Installation of dokcer
login into docker hub using valid credential

$ docker login
username:
password:

# STEP-2 Install Docker Compose (select Install Compose -> Linux) #
Follow this instruction https://docs.docker.com/compose/install/

# STEP-3 Install minikube as lightweight kubernetes distribution #
Follow this instruction https://minikube.sigs.k8s.io/docs/start/

# STEP-3 Install kubeclt (as root user) #
Follow this instruction https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

After done Installation of Kubectl as root user

$ logout

Run minikube as non root user with 1 master and 2 nodes

$ minikube start --nodes 3 -p multinode

# STEP-4 Frok Repository, setting up jenkins home directory and deploy jenkins container #
$ apt install git -y

$ git clone https://JohnSujoy@bitbucket.org/JohnSujoy/bs-23_task1.git

$ cd bs-23_task1/infra/config

$ bash bash ./jenkins.sh

# STEP-5 Setting up jenkins #
$ sudo docker ps

Check jenkins conatainer status up and running

After spin up the jenkins container hit this url: http://127.0.0.1:8080

$ sudo docker logs -f <dockerID>

Copy jekins secret token from terminal and continue

Select -> Install suggested pulgins -> Continue

Setting up Admin user account for jenkins and continue

Continue with this jenkins URL http://127.0.0.1:8080 and start using jenkins

# STEP-6 Create pipeline for Laravel-app1 #
From dashboard select -> New Item -> Enater Name 'Laravel-app1' -> Select pipeline -> ok

$ cd bs-23_task1/app1

$ cat Jenkinsfile

Copy all lines of Jenkins file and paste it Pipeline Script section of Laravel-app1 project -> Save

# STEP-7 Create pipeline for Laravel-app2 #
From dashboard select -> New Item -> Enater Name 'Laravel-app2' -> Select pipeline -> ok

$ cd bs-23_task1/app2

$ cat Jenkinsfile

Copy all lines of Jenkins file and paste it Pipeline Script section of Laravel-app2 project -> Save


# STEP-8 Trigger jenkins for Laravel-app1 pipeline #
Go to jenkins dashboard -> select Laravel-app1 -> click Build Now

# STEP-9 Trigger jenkins for Laravel-app2 pipeline #
Go to jenkins dashboard -> select Laravel-app2 -> click Build Now

# STEP-10 Testing #
$ sudo su

kubectl get po (run as root user)

Check laravel-app1, laravel-app2 and nginx status is running

Run minikube as non root user

$ minikube service nginx

Their will be a URL open automaticaly using your browser or show a URL with http://nodeip:nodeport

Copy this url and open with nrowser

Try this url pattern

http://nodeip:nodeport/app1 -> Hello I am App 1

http://nodeip:nodeport/app2 -> Hello I am App 2
